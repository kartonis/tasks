#include "module1.h"
#include "module2.h"
#include "module3.h"
#include <iostream>
using std:: cout;
int main()
{
    cout <<  "Hello world!" << "\n";
    
    cout << Module1::getMyName() << "\n";
    cout << Module2::getMyName() << "\n";
    
    using namespace Module1;
    cout << getMyName() << "\n";
    cout << Module2::getMyName() << "\n";
    
    //using namespace Module2;//конфликт пространств имён
    //std::cout << getMyName() << "\n";
    
    using Module2::getMyName;
    cout << getMyName() << "\n";
    cout << Module3::getMyName()<< "\n";
    
}

